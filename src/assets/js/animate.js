import { TweenLite } from 'gsap';

// /**
//  * parallax ตัวแปรต่างๆ
//  * @param {selector} obj - mouse detect on obj (selector)
//  * @param {selector} target - target to parallax (selector)
//  * @param {string} type - type of target (margin, padding, background-position)
//  * @param {int} amount - amount of parallax (amount) default = 10
//  * @param {boolean} reverse - rerverse parallax (true, false) default = true
//  * @param {boolean} scroll - enable parallax when scroll (true, false) default = false
//  */
var parallax2 = function(obj, target, type, amount = 10, scrollAmount = 90, reverse = true, scroll = false, add = 1) {
  {
    var posY;
    if (scroll) {
      const posY2 = $(window).scrollTop() / scrollAmount;
      if (reverse) {
        target.css(type, 0 + 'px ' + posY2 * add + 'px ');
      } else {
        target.css(type, posY2 * add + 'px 0px');
      }
      $(window).resize(function() {
        const posY2 = $(window).scrollTop() / scrollAmount;
        if (reverse) {
          target.css(type, 0 + 'px ' + posY2 * add + 'px ');
        } else {
          target.css(type, posY2 * add + 'px 0px');
        }
      });
      $(document).on('scroll', function(e) {
        const posY2 = $(window).scrollTop() / scrollAmount;
        if (reverse) {
          target.css(type, 0 + 'px ' + posY2 * add + 'px ');
        } else {
          target.css(type, posY2 * add + 'px 0px');
        }
      });
    }
  }
};
var parallax = function(
  obj,
  target,
  type,
  amount = 10,
  scrollAmount = 90,
  reverse = true,
  scroll = false,
  mouse = true
) {
  var posX;
  var posY;
  if (mouse) {
    $(obj).on('mousemove', function(e) {
      posX = e.clientX * (reverse ? -1 : 1) / amount;
      posY = e.clientY * (reverse ? -1 : 1) / amount;
      $(target).css(type, posX + 'px ' + posY + 'px ');
    });
  }
  if (scroll) {
    $(document).on('scroll', function(e) {
      const posY2 = $(window).scrollTop() / scrollAmount;
      $(target).css(type, 0 + 'px ' + posY2 * -1 + 'px ');
    });
  }
};

var scrollTo = function(target, offsetY = 0) {
  setTimeout(
    () => {
      if ($(target).length) {
        TweenLite.to($(window), 1, { scrollTo: { y: target, offsetY: offsetY } });
      } else {
        throw "can't find element to scroll to.";
      }
    },
    100,
    1
  );
};

export { parallax, parallax2, scrollTo };
