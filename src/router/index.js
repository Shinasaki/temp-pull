// App Section
import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store/main.js';
import {
  mapActions
} from 'vuex';
import swal from 'sweetalert2';

// LAYOUT
const Header = () =>
  import ('@components/Layouts/Header');
const Body = () =>
  import ('@components/Layouts/Body');
const Backend = () =>
  import ('@components/Layouts/Backend');

// BODY
const Index = () =>
  import ('@components/Bodys/Index');
const Register = () =>
  import ('@components/Bodys/Register/Register');
const Notfound = () =>
  import ('@components/Bodys/Notfound');
const Announce = () =>
  import ('@components/Bodys/Announce/Announce');
const Announce_Login = () =>
  import ('@components/Bodys/Announce/_Login');
const Announce_Upload = () =>
  import ('@components/Bodys/Announce/_Upload');
const Announce_Summary = () =>
  import ('@components/Bodys/Announce/_Summary');

// -- Register
const Reg = () =>
  import ('@components/Bodys/Register/Register');
const Reg_Auth = () =>
  import ('@components/Bodys/Register/_Auth');
const Reg_Camp = () =>
  import ('@components/Bodys/Register/_Camp');
const Reg_Personal = () =>
  import ('@components/Bodys/Register/_Personal');
const Reg_SubCamp = () =>
  import ('@components/Bodys/Register/_SubCamp');
const Reg_Summary = () =>
  import ('@components/Bodys/Register/_Summary');
const Reg_Subcamp_Game = () =>
  import ('@components/Bodys/Register/subcamp/_Game');
const Reg_Subcamp_Iot = () =>
  import ('@components/Bodys/Register/subcamp/_Iot');
const Reg_Subcamp_Network = () =>
  import ('@components/Bodys/Register/subcamp/_Network');
const Reg_Subcamp_App = () =>
  import ('@components/Bodys/Register/subcamp/_App');
const Reg_Subcamp_Data = () =>
  import ('@components/Bodys/Register/subcamp/_Data');


// BACKEND
// const Backend_Index = () => import('@components/Backend/Index');
// const Backend_Login = () => import('@components/Backend/Login');
// const Account_Applicant = () => import('@components/Backend/Account_Applicant');
// const Account_Staff = () => import('@components/Backend/Account_Staff');
// const Check_Answer = () => import('@components/Backend/Check_Answer');
// const Check_Applicant = () => import('@components/Backend/Check_Applicant');
// const Choose_Applicant = () => import('@components/Backend/Choose_Applicant');
// const Question_Applicant = () => import('@components/Backend/Question_Applicant');
// const Question_Camp = () => import('@components/Backend/Question_Camp');

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/14',
    components: {
      header: Header,
      body: Body
    },
    children: [{
        path: '404',
        component: Notfound,
        name: 'Notfound'
      },
      {
        path: '',
        component: Index,
        name: 'Index'
      },
      // {
      //   path: 'announce',
      //   component: Announce,
      //   children: [{
      //       path: 'login',
      //       component: Announce_Login,
      //       name: 'Announce_Login',
      //       meta: {
      //         annNotAuth: true
      //       }
      //     },
      //     {
      //       path: 'upload',
      //       component: Announce_Upload,
      //       name: 'Announce_Upload',
      //       meta: {
      //         annAuth: true,
      //         annUserSelected: true
      //       }
      //     },
      //     {
      //       path: 'summary',
      //       component: Announce_Summary,
      //       name: 'Announce_Summary',
      //       meta: {
      //         annAuth: true,
      //         annUserSelected: true
      //       }
      //     }
      //   ]
      // }
      // {
      //   path: 'register',
      //   component: Register,
      //   name: 'Register',
      //   children: [
      //     {
      //       path: 'auth',
      //       component: Reg_Auth,
      //       name: 'Reg_Auth',
      //       alias: '/',
      //       meta: { notAuth: true }
      //     },
      //     {
      //       path: 'camp',
      //       component: Reg_Camp,
      //       name: 'Reg_Camp',
      //       meta: { requiresAuth: true, progress2: true, notSubmit: true }
      //     },
      //     {
      //       path: 'personal',
      //       component: Reg_Personal,
      //       name: 'Reg_Personal',
      //       meta: { requiresAuth: true, progress1: true, notSubmit: true }
      //     },
      //     {
      //       path: 'summary',
      //       component: Reg_Summary,
      //       name: 'Reg_Summary',
      //       meta: { requiresAuth: true, Submit: true }
      //     },
      //     {
      //       path: 'subcamp',
      //       component: Reg_SubCamp,
      //       name: 'Reg_SubCamp',
      //       meta: { requiresAuth: true, progress3: true, notSubmit: true },
      //       children: [
      //         { path: 'game', component: Reg_Subcamp_Game, name: 'Reg_Subcamp_Game', alias: '/' },
      //         { path: 'iot', component: Reg_Subcamp_Iot, name: 'Reg_Subcamp_Iot' },
      //         { path: 'network', component: Reg_Subcamp_Network, name: 'Reg_Subcamp_Network' },
      //         { path: 'app', component: Reg_Subcamp_App, name: 'Reg_Subcamp_App' },
      //         { path: 'data', component: Reg_Subcamp_Data, name: 'Reg_Subcamp_Data' }
      //       ]
      //     }
      //   ]
      // }
      // {
      //   path: 'backend',
      //   component: Backend,
      //   name: 'Backend',
      //   children: [
      //     { path: 'login', component: Backend_Login, name: 'Backend_Login', alias: '/' },
      //     { path: 'index', component: Backend_Index, name: 'Backend_Index' },
      //     { path: 'account-applicant', component: Account_Applicant, name: 'Backend_Account_Applicant' },
      //     { path: 'account-staff', component: Account_Staff, name: 'Backend_Account_Staff' },
      //     { path: 'check-answer', component: Check_Answer, name: 'Backend_Check_Answer' },
      //     { path: 'check-applicant', component: Check_Applicant, name: 'Backend_Check_Applicant' },
      //     { path: 'choose-applicant', component: Choose_Applicant, name: 'Backend_Choose_Applicant' },
      //     { path: 'question-applicant', component: Question_Applicant, name: 'Backend_Question_Applicant' },
      //     { path: 'question-camp', component: Question_Camp, name: 'Backend_Question_Camp' }
      //   ]
      // }
    ]
  }]
});

// router.beforeEach(function(to, from, next) {
//   if (to.matched.some((record) => record.meta.progress1)) {
//     !store.getters.getUserProgress.submit ? next() : next({ name: 'Reg_Summary' });
//   } else if (to.matched.some((record) => record.meta.progress2)) {
//     !store.getters.getUserProgress.submit ? next() : next({ name: 'Reg_Summary' });
//   } else if (to.matched.some((record) => record.meta.progress3)) {
//     !store.getters.getUserProgress.submit ? next() : next({ name: 'Reg_Summary' });
//   } else {

//   }
// });
if (store.getters.getUserLogin == false) {
  (store.state.user = {
    login: false
  }), (store.state.submit = {}), (store.state.userProgress = {});
}
router.beforeResolve(function (to, from, next) {
  to.matched.length ? next() : next({
    name: 'Index'
  });
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    !store.getters.getUserLogin ? next({
      name: 'Reg_Auth'
    }) : '';
  } else if (to.matched.some((record) => record.meta.notAuth)) {
    store.getters.getUserLogin ? next({
      name: 'Reg_Personal'
    }) : next();
  }
  if (to.matched.some((record) => record.meta.notSubmit)) {
    store.getters.getUserProgress.submit ? next({
      name: 'Reg_Summary'
    }) : '';
  }
  if (to.matched.some((record) => record.meta.submit)) {
    !store.getters.getUserProgress ? next('Reg_Summary') : '';
  }
  if (to.matched.some((record) => record.meta.progress2)) {
    store.getters.getUserProgress.progress < 0 ? next({
      name: 'Reg_Personal'
    }) : next();
  } else if (to.matched.some((record) => record.meta.progress3)) {
    store.getters.getUserProgress.progress < 1 ? next({
      name: 'Reg_Camp'
    }) : next();
  }

  if (to.matched.some((record) => record.meta.annAuth)) {
    !store.getters.getUserLogin ? next({
      name: 'Announce_Login'
    }) : '';
  } else if (to.matched.some((record) => record.meta.annNotAuth)) {
    store.getters.getUserLogin ? next({
      name: 'Announce_Upload'
    }) : next();
  }

  if (to.matched.some((record) => record.meta.annUserSelected)) {
    var info = store.getters.getUserProgress;
    if (info.status === 'CANCEL') {
      swal({
        type: 'info',
        title: 'ไม่สามารถเข้าถึงได้',
        text: 'น้องได้สละสิทธิ์การเข้าร่วม ITCAMP ครั้งที่ 14 ไปแล้วค่ะ.'
      });
      return next({
        name: 'Index'
      });
    } else if (info.status === 'CONFIRM') {
      return next({
        name: 'Announce_Summary'
      });
    } else if (info.status === 'SELECT') {
      swal({
        type: 'info',
        title: 'ขอแสดงความยินดี',
        text: 'น้อง ๆ ได้ผ่านการคัดเลือกให้เข้าร่วมทีมสำรวจอวกาศไปกับยาน CampXIV แห่งค่ายไอทีแคมป์ครั้งที่ 14'
      });
      next();
    } else if (info.status === 'RESERVE') {
      swal({
        type: 'info',
        title: 'ลุ้นอีกหน่อยนะครับ',
        html: 'ถ้าน้องได้รับสิทธิ์ต่อจากเพื่อน พี่ ๆ จะติดต่อกลับไป <br>ภายในวันที่ 20 พฤษภาคม พ.ศ. 2561"'
      });
      return next({
        name: 'Index'
      });
    } else {
      swal({
        type: 'info',
        title: 'ขอแสดงความเสียใจ',
        html: 'น้องไม่ได้ถูกคัดเลือกเข้าค่าย ITCAMP ครั้งที่ 14 <br>ขอแสดงความเสียใจด้วยค่ะ T^T '
      });
      return next({
        name: 'Index'
      })
    }
  }
});
export default router;